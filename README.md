# Scraper template repository with anything required to start building a project. 

![Playwright vs Puppeteer](https://www.browserless.io/static/55eb4e1e82f57c54ed8c9397da22f835/8117f/Puppeteer-vs-Playwright-Browser-automation-1.png)

## Steps to create it
### Git
git init
touch .gitignore
npm init

### Dependencies
npm install puppeteer, playwright, eslint --save

#### ESLint init
npx eslint --init

### Dev Dependencies
npm install -D typescript


## TODO
- consider using yarn, pnpm
- consider using ts-node, tsx

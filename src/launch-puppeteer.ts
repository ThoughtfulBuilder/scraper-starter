import puppeteer from 'puppeteer'
import dotenv from 'dotenv'

// Load environment variables from .env file

void (async () => {
  dotenv.config()
  const headless = process.env.HEADLESS === 'true'
  const browser = await puppeteer.launch({ headless })
  const page = await browser.newPage()

  await page.goto('https://example.com')
  await page.setViewport({ width: 1080, height: 1024 })

  const titleSelector = 'title'
  const textSelector = await page.waitForSelector(titleSelector)
  const fullTitle = await textSelector?.evaluate(el => el.textContent)

  console.log('The title of example.com is "%s".', fullTitle)

  await browser.close()
})()
